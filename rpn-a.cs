﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace rpn
{
    class Program
    {
        static void Main(string[] args)
        {
            //Power p = new Power();
            //int tmp = 0;
            //if (int.TryParse(Console.ReadLine(), out tmp))
            //{

            //    Stack<int> s = new Stack<int>();
            //    s.Push(1);
            //    s.Push(tmp);
            //    p.Calculate(s);
            //    Console.WriteLine(s.Pop());
            //}
            var programs = new List<string>
            {
                "1",
                "0 2 +",
                "2 3 * 3 -",
                "20 2 4 ^ -", //                "20 2 4^-",

                "   13 7 + 5   1 - /   ", //                 "   13 7+5   1 -/   ",

                "5 0 ~ ! +",
                "\t3\n\t4\n\t+\n",
                "0 2 - 3 ^",
                "0 3 - 2 ^",
                "3 8 - 0 2 - *",
                "0 3 - ! ",
                "13 0 3 - ^",
                "2147483647",
            };

            Calculator calc = new Calculator();

            foreach (var p in programs)
            {
                Console.WriteLine(p);

                try
                {
                    Console.WriteLine("RESULT: " + calc.Calculate(p));
                }
                catch
                {
                    Console.WriteLine("NO RESULT");
                }

                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }

    class Calculator
    {
        Stack<int> memory;
        Tokenizer tokenizer;

        public Calculator()
        {
            memory = new Stack<int>();
            tokenizer = new Tokenizer();
        }

        public int Calculate(string expression)
        {
            List<IExpression> tokens = tokenizer.Parse(expression);

            foreach (var t in tokens)
            {
                t.Calculate(memory);
            }

            return memory.Pop();
        }

    }

    class Tokenizer
    {
        public List<IExpression> Parse(string expression)
        {
            Dictionary<string, IExpression> d = new Dictionary<string, IExpression>();
            // dodawanie operacji
            {
                d.Add("+", new Add());
                d.Add("-", new Substract());
                d.Add("*", new Multiply());
                d.Add("/", new Divide());
                d.Add("^", new Power());
                d.Add("~", new Not());
                d.Add("!", new Factorial());
                //d.Add("0", new Number());
            }
            List<string> keys = new List<string>();
            foreach (var k in d.Keys)
                keys.Add(k);
            var keys_ = keys.ToArray();
            //Regex regex = new Regex(@"\s");
            List<IExpression> tokens = new List<IExpression>();
            string[] s = expression.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            Stack<int> memory = new Stack<int>();

            for(int i = 0; i < s.Length; ++i)
            {
                int p;
                if (int.TryParse(s[i], out p))
                {
                    //memory.Push(p);
                    tokens.Add(new Number(p));
                }
                else
                {
                    IExpression tmp;
                    d.TryGetValue(s[i][0].ToString(), out tmp);
                    tokens.Add(tmp);
                    //tmp.Calculate(memory);
                }
            }
                                                                                                                   
            //TODO create tokens

            return tokens;
        }

        //public static IList<string> SplitAndKeepDelimiters(this string s, string[] delimiters)
        //{
        //    var parts = new List<string>();
        //    List<char> tmp1 = new List<char>();
        //    foreach (string st in delimiters)
        //        tmp1.Add(st[0]);
        //    char[] tmp2 = tmp1.ToArray();
        //    if (!string.IsNullOrEmpty(s))
        //    {
        //        int iFirst = 0;
        //        do
        //        {
        //            int iLast = s.IndexOfAny(tmp2, iFirst);
        //            if (iLast >= 0)
        //            {
        //                if (iLast > iFirst)
        //                    parts.Add(s.Substring(iFirst, iLast - iFirst)); //part before the delimiter
        //                parts.Add(new string(s[iLast], 1));//the delimiter
        //                iFirst = iLast + 1;
        //                continue;
        //            }

        //            //No delimiters were found, but at least one character remains. Add the rest and stop.
        //            parts.Add(s.Substring(iFirst, s.Length - iFirst));
        //            break;

        //        } while (iFirst < s.Length);
        //    }

        //    return parts;
        //}
    }

    interface IExpression
    {
        void Calculate(Stack<int> memory);
    }

    class Number : IExpression
    {
        int value;
        public Number(int _value) { value = _value; }
        public void Calculate(Stack<int> memory)
        {
            memory.Push(value);
        }
    }

    class Add : IExpression
    {
        public void Calculate(Stack<int> memory)
        {
            //Console.WriteLine("lmao");
            int a = memory.Pop();
            int b = memory.Pop();
            memory.Push(a + b);
            //Console.WriteLine("{0} + {1} == {2}", a, b, a + b);
        }
    }

    class Substract : IExpression
    {
        public void Calculate(Stack<int> memory)
        {
            int b = memory.Pop();
            int a = memory.Pop();
            memory.Push(a - b);
            //Console.WriteLine("{0} - {1} == {2}", a, b, a - b);
        }
    }

    class Divide : IExpression
    {
        public void Calculate(Stack<int> memory)
        {
            int b = memory.Pop();
            int a = memory.Pop();
            memory.Push(a / b);
            //Console.WriteLine("{0} / {1} == {2}", a, b, a / b);

        }
    }

    class Multiply : IExpression
    {
        public void Calculate(Stack<int> memory)
        {
            memory.Push(memory.Pop() * memory.Pop());
        }
    }

    class lessThanZero : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return (y > 0);
        }

        public int GetHashCode(int obj)
        {
            return obj;
        }
    }

    class Power : IExpression
    {
        Dictionary<int, int> c = new Dictionary<int, int>(new lessThanZero());
        public Power()
        {
            //c.Add(-1, 0);
        }
        public void Calculate(Stack<int> memory)
        {
            int pow = memory.Pop();
            int a = memory.Pop();
            c.Add(pow, pow);
            int tmp;
            c.TryGetValue(pow, out tmp);
            c.Clear();
            memory.Push((int)Math.Pow(a, tmp));

            //memory.Push(tmp);
        }
    }

    class equalsZero : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return (x == y);
        }

        public int GetHashCode(int obj)
        {
            return obj;
        }
    }

    


    class Not : IExpression
    {
        Dictionary<int, int> c = new Dictionary<int, int>(new equalsZero());
        public Not()
        {
            c.Add(0, 1);
        }
        public void Calculate(Stack<int> memory)
        {
            int a = memory.Pop();
            //c.Add(a, a);
            int tmp;
            c.TryGetValue(a, out tmp);
            memory.Push(tmp);
        }
    }

    class Factorial : IExpression
    {
        Dictionary<int, int> c = new Dictionary<int, int>(new lessThanZero());
        public void Calculate(Stack<int> memory)
        {
            int a = memory.Pop();
            c.Add(a, a);
            int tmp;
            c.TryGetValue(a, out tmp);
            int value = 1;
            for (int i = 1; i <= tmp; ++i)
                value *= i;
            c.Clear();
            memory.Push(value);
        }
    }

    public enum Condition
    {
        
    }

}
